---
title: Achtste dag 
subtitle: Door Ylva Wolter
date: 2017-09-14
bigimg: [{src: "/img/path.jpg", desc: "Path"}]
---

**Blogposts**
-------------

*Donderdag 14  September 2017*

> Vandaag weer een super vermoeiende dag, eerst een keuzevak (Tools for design) om 08:30. En dan heel lang tussenuur, iets van twee uur. In die twee uur hebben we weer verder gewerkt aan de paperprototype en daarna hebben we de spelanalyse gedaan in de stad (wel nadat we lekker een tosti hadden gegeten). 
>  
>  De spelanalyse was wel leuk en heel interessant om te zien hoe het spel nu echt werkt. We hebben hier wel een aantal kleine aanpassingen gemaakt. 
>   
>   Na die twee uur hadden we werkcollege, dat sloot goed aan op het hoorcollege en een workshop die ik gevolgd had (beeldanalyse). We moesten foto's beoordelen, door de theorie te gebruiken van het hoorcollege, ik vond dit wel moeilijk want ik was er niet met het hoorcollege. Gelukkig was er ook een blaadje met veel informatie uitgedeeld, daar kon ik wel mee aan de slag.