---
title: Zevende dag 
subtitle: Door Ylva Wolter
date: 2017-09-13
bigimg: [{src: "/img/path.jpg", desc: "Path"}]
---


**Blogposts**
-------------

*Woensdag 13  September 2017*

> Gister was ik helaas ziek, ik had heel erg hoofdpijn en het werd alleen maar erger dus ik ben thuis gebleven. Ik ga de hoorcollege terugluisteren op n@tschool.
> Vandaag hebben we hard verder gewerkt aan de paperprototype. Zo hebben we de app en de spellen uitgeprint en in elkaar gezet. Aan het einde van de ochtend hebben verschillende teams hun prototypes aan elkaar gepresenteerd. Hierbij kreeg elk team Tips&Tops, onze tips waren:
>  
> - Denk beter na over het doel van de Mol
> - Het is iets te onduidelijk, versimpel het spel
>  
>  We kregen ook tops natuurlijk:
>  
>  - Studenten leren elkaar goed kennen
>  - Je ziet veel van Rotterdam
>   
>   We gaan aan de slag met de tips, we denken dat er ook enigszins onduidelijkheid onstond door onze chaotische manier van presenteren dus daar moeten we ook aan gaan werken.
> 
> Later op de dag hadden we een workshop over Beeld en het analyseren van een beeld. Dit was erg interessant. Zo heb ik veel nieuws geleerd over composities en contrasten.
>  
>  Hierna zijn we weer verder gaan werken aan de paperprototype.