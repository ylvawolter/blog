---
title: Vijfde dag
subtitle: Door Ylva Wolter
date: 2017-09-11
bigimg: [{src: "/img/path.jpg", desc: "Path"}]
---

**Blogposts**
-------------

*Maandag 11  September 2017*

> Ik had eigenlijk helemaal niet zo'n zin in vandaag, omdat het voor mij echt een super lange vermoeiende dag is. Toch zal ik er maar aan gaan moeten wennen want het rooster zal zo blijven voor een aantal weken maar daarnaast moet ik ook nog eens werken na school, wat het nog vermoeiender maakt. Geld moet rollen, ik ook vanmorgen, uit m'n bed. 
>  
>  Ah fijn, we begonnen de les met een kleine instructie en uitleg over hoe de dag verder zou verlopen. Ik vond het  we gingen weer verder aan onze Design Challenge, waar ik nog steeds een goed gevoel over heb. We zijn richting het einde van de dag met een goed concept gekomen wat gebaseerd is op "Wie Is De Mol?", we moeten het nog wel wat verder uitvogelen maar voor zover klonk het heel goed.
>   
>   We zijn wel aan de slag gegaan met de paperprototype en de spelregels vast te leggen. 
>    
>    Hierna kregen we feedback van onze mentor:We kregen nog feedback van onze mentor:
>     
>     - Denk aan de magic circle 
>     - Vergeet de meaningfull play niet
>     - Misschien terugknoppen toevoegen

